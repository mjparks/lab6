/******************* 
  Michael J. Parks 
  mjparks       
  Lab 5              
  Lab Section: 2
  Hanjie Liu           
*******************/

#include <iostream>
#include <iomanip>
#include <algorithm>
#include <sstream>
#include <string>
#include "stdlib.h"
#include "string.h"
using namespace std;
enum Suit { SPADES=0, HEARTS=1, DIAMONDS=2, CLUBS=3 };

typedef struct Card {
  Suit suit;
  int value;
} Card;

string get_suit_code(Card& c);
string get_card_name(Card& c);
bool suit_order(const Card& lhs, const Card& rhs);
int myrandom (int i) { return std::rand()%i;}


int main(int argc, char const *argv[]) {
  // IMPLEMENT as instructed below
  /*This is to seed the random generator */
  srand(unsigned (time(0)));

  /*Create a deck of cards of size 52 (hint this should be an array) and
   *initialize the deck*/
  int i = 0;

  Card cardDeck[52];
  for (i = 0; i < 52; i++) {
	if (i <= 12) {
		cardDeck[i].value = (i % 13) + 2;
		cardDeck[i].suit = SPADES;
	} else if (i > 12 && i <= 25) {
		cardDeck[i].value = (i % 13) + 2;
		cardDeck[i].suit = HEARTS;
	} else if (i > 25 && i <= 38) {
		cardDeck[i].value = (i % 13) + 2;
		cardDeck[i].suit = DIAMONDS;
	} else if (i > 38) {
		cardDeck[i].value = (i % 13) + 2;
		cardDeck[i].suit = CLUBS;
	}
  }

  /*After the deck is created and initialzed we call random_shuffle() see the
   *notes to determine the parameters to pass in.*/
  random_shuffle(&cardDeck[0], &cardDeck[52], myrandom);

   /*Build a hand of 5 cards from the first five cards of the deck created
    *above*/
  Card hand[5];
  for (i = 0; i < 5; i++) {
	hand[i] = cardDeck[i];
  }

    /*Sort the cards.  Links to how to call this function is in the specs
     *provided*/
  sort(hand, hand + 5, suit_order);  

    /*Now print the hand below. You will use the functions get_card_name and
     *get_suit_code */
  for (i = 0; i < 5; i++) {
	cout << setw(10) << right << get_card_name(hand[i]) << " of " 
		<< get_suit_code(hand[i]) << endl;
  }

  return 0;
}


/*This function will be passed to the sort funtion. Hints on how to implement
* this is in the specifications document.*/
bool suit_order(const Card& lhs, const Card& rhs) {
  if (lhs.suit < rhs.suit) {
	return true;
  } else if (lhs.suit == rhs.suit) {
	if (lhs.value < rhs.value) {
		return true;
	} else {
		return false;
	}
  } else {
	return false;
  }
}

string get_suit_code(Card& c) {
  switch (c.suit) {
    case SPADES:    return "\u2660";
    case HEARTS:    return "\u2661";
    case DIAMONDS:  return "\u2662";
    case CLUBS:     return "\u2663";
    default:        return "";
  }
}

string get_card_name(Card& c) {
  string cardName;
  if (c.value >=2 && c.value <= 10) {
  	cardName = to_string(c.value);
  } else {
	switch (c.value) {
		case 11:
			cardName = "Jack";
			break;
		case 12:
			cardName = "Queen";
			break;
		case 13:
			cardName = "King";
			break;
		case 14:
			cardName = "Ace";
			break;
	}
  }
  return cardName;
}
